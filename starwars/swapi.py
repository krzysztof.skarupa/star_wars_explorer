from typing import Dict, List

import dateutil.parser
import requests
from django.conf import settings

from starwars.exceptions import WrongSWAPIStatusCodeException


def make_api_call(url: str):
    response = requests.get(url)
    if response.status_code != 200:
        raise WrongSWAPIStatusCodeException(url, response.status_code)
    return response.json()


def fetch_people_data_from_api():
    next_url = settings.PEOPLE_API_URL
    people_data = []
    while next_url:
        response_data = make_api_call(next_url)
        people_data.extend(response_data['results'])
        next_url = response_data['next']

    return people_data


def prepare_date(date_str: str):
    return dateutil.parser.parse(date_str).strftime('%Y-%m-%Y')


def prepare_single_person_data(person_data: Dict, planets_fetcher):
    person_data['date'] = prepare_date(person_data['edited'])
    person_data['homeworld'] = planets_fetcher.resolve_planet_name(
        person_data['homeworld']
    )
    del person_data['films']
    del person_data['vehicles']
    del person_data['starships']
    del person_data['species']
    del person_data['url']
    del person_data['created']
    del person_data['edited']
    return person_data


def process_people_data(people_list: List):
    planets_fetcher = PlanetsFetcher()
    return [
        prepare_single_person_data(person, planets_fetcher)
        for person in people_list
    ]


def get_people_data():
    people_data = fetch_people_data_from_api()
    return process_people_data(people_data)


class PlanetsFetcher:
    def __init__(self):
        self._planet_names = {}
        next_url = settings.PLANETS_API_URL

        while next_url:
            response_data = make_api_call(next_url)
            next_url = response_data['next']
            for planet in response_data['results']:
                self._planet_names[planet['url']] = planet['name']

    def resolve_planet_name(self, url: str):
        if url in self._planet_names:
            return self._planet_names[url]

        planet_name = self._fetch_planet_name_from_api(url)
        return planet_name

    def _fetch_planet_name_from_api(self, url: str):
        """Fetches a planet name from the API, saves it to the internal
        database and returns it.
        """
        response_data = make_api_call(url)
        name = response_data.get('name')
        self._planet_names[url] = name
        return name
