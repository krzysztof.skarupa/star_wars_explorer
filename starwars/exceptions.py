class WrongSWAPIStatusCodeException(Exception):
    def __init__(self, url: str, status_code: int):
        message = f'Request for {url} returned status code {status_code}'
        super().__init__(message)
