import petl
from django.shortcuts import get_object_or_404
from django.views.generic import ListView, RedirectView, TemplateView

from starwars.models import DataCollection


class IndexView(ListView):
    template_name = 'starwars/index.html'
    queryset = DataCollection.objects.order_by('-date_added')
    paginate_by = 10


class NewCollectionView(RedirectView):
    pattern_name = 'index'

    def get(self, request, *args, **kwargs):
        DataCollection.new_collection()
        return super().get(request, *args, **kwargs)


class InspectionView(TemplateView):
    template_name = 'starwars/inspect.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        collection_id = kwargs['id']
        collection = get_object_or_404(DataCollection, id=collection_id)

        try:
            show_rows = int(self.request.GET.get('show_rows', 10))
        except ValueError:
            show_rows = 10

        header, body = collection.get_collection_data(show_rows)

        context['collection'] = collection
        context['header'] = header
        context['body'] = body

        if len(body) == show_rows:
            context['next_show_rows'] = show_rows + 10
        else:
            context['next_show_rows'] = None

        return context


class ValueCountView(TemplateView):
    template_name = 'starwars/value_count.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        collection_id = kwargs['id']
        collection = get_object_or_404(DataCollection, id=collection_id)
        full_data = collection.load_data_from_file()

        selected_columns = self.request.GET.get('columns', '').split(',')
        selected_columns = [column for column in selected_columns if
                            column in full_data[0]]
        table_header = selected_columns + ['Count']
        all_headers = [{
            'header': header,
            'link': self.get_link_for_header(header,
                                             selected_columns)
        } for header in full_data[0]]

        if selected_columns:
            counter = petl.valuecounter(full_data, *selected_columns)
            table_body = []
            for k, v in counter.items():
                if isinstance(k, tuple):
                    table_body.append(list(k) + [v])
                else:
                    table_body.append([k, v])
        else:
            table_body = None

        context['collection'] = collection
        context['selected_columns'] = selected_columns
        context['table_header'] = table_header
        context['table_body'] = table_body
        context['all_headers'] = all_headers
        return context

    def get_link_for_header(self, header, selected_columns):
        if header in selected_columns:
            headers = [h for h in selected_columns if h != header]
        else:
            headers = selected_columns + [header]
        headers_str = ','.join(headers)

        return f'?columns={headers_str}'
