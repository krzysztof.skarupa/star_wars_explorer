import csv
import datetime
import io

from django.db import models

from starwars import swapi


class DataCollection(models.Model):
    date_added = models.DateTimeField(auto_now_add=True)
    file = models.FileField(upload_to='collections')

    @classmethod
    def new_collection(cls):
        people_data = swapi.get_people_data()

        timestamp = datetime.datetime.now().timestamp()
        filename = f'{timestamp}.csv'
        file_obj = io.StringIO()
        fieldnames = list(people_data[0].keys())

        writer = csv.DictWriter(file_obj, fieldnames)
        writer.writeheader()
        writer.writerows(people_data)

        collection = cls()
        collection.file.save(filename, file_obj)
        collection.save()

    def load_data_from_file(self):
        with open(self.file.path) as fil:
            reader = csv.reader(fil)
            data = list(reader)

        return data

    def get_collection_data(self, rows=None):
        data = self.load_data_from_file()
        header = data[0]
        if rows is not None:
            body = data[1: rows + 1]
        else:
            body = data[1:]
        return header, body
