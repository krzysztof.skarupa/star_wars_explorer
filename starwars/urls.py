from django.urls import path

from starwars.views import IndexView, InspectionView, NewCollectionView, ValueCountView

urlpatterns = [
    path('', IndexView.as_view(), name='index'),
    path('new-collection', NewCollectionView.as_view(), name='new_collection'),
    path('inspect/<id>', InspectionView.as_view(), name='inspection'),
    path('inspect/<id>/value-count', ValueCountView.as_view(), name='value_count'),
]
